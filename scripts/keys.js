let data = `backspace	8
						tab	9
						enter	13
						shift	16
						ctrl	17
						alt	18
						pause_break	19
						caps_lock	20
						escape	27
						spacebar	32
						page_up	33
						page_down	34
						end	35
						home	36
						left_arrow	37
						up_arrow	38
						right_arrow	39
						down_arrow	40
						insert	45
						delete	46
						0	48
						1	49
						2	50
						3	51
						4	52
						5	53
						6	54
						7	55
						8	56
						9	57
						A	65
						B	66
						C	67
						D	68
						E	69
						F	70
						G	71
						H	72
						I	73
						J	74
						K	75
						L	76
						M	77
						N	78
						O	79
						P	80
						Q	81
						R	82
						S	83
						T	84
						U	85
						V	86
						W	87
						X	88
						Y	89
						Z	90
						left_window	91
						right_window	92
						select	93
						numpad_0	96
						numpad_1	97
						numpad_2	98
						numpad_3	99
						numpad_4	100
						numpad_5	101
						numpad_6	102
						numpad_7	103
						numpad_8	104
						numpad_9	105
						multiply	106
						add	107
						subtrack	109
						decimal_point	110
						divide	111
						f1	112
						f2	113
						f3	114
						f4	115
						f5	116
						f6	117
						f7	118
						f8	119
						f9	120
						f10	121
						f11	122
						f12	123
						num_lock	144
						scroll_lock	145
						semi-colon	186
						equal_sign	187
						comma	188
						dash	189
						period	190
						forward_slash	191
						grave_accent	192
						open_bracket	219
						back_slash	220
						close_bracket	221
						single_quote	222`;

// формирование ассоциативного массива
// ключ - код клавиши
// значение - название клавиши
let parse_keys = (data) => {
	let a = Array.prototype.concat.apply([], data.split("\n").map((i) => i.split("	")) );
	let b = {};
	a.forEach((item, i, a) => {
		if(i % 2 == 0){
			b[a[i+1]]=a[i]; 
		}
	});
	return b;
}

const KEYS = parse_keys(data);

// клавиши модификаторы
const MODIFIER_KEYS = [16, 17, 18, 91]; // переделать 