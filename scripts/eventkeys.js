window.addEventListener("load", loadKeyboard);

function loadKeyboard(){
	document.onkeydown = function(e){
		setAtr(e,"class","button_press");
		return false;
	};

	document.onkeyup = function(e){
		setAtr(e,"class","button");
		return false;
	};
}

function setAtr(e,atr,value){
	let key;
	let keyc = e.keyCode;
	if(keyc == 16 || keyc == 17 || keyc == 18) {
		//left
		key = document.getElementById(("l"+KEYS[keyc]).toLowerCase());
		key.setAttribute(atr, value);
		//right
		key = document.getElementById(("r"+KEYS[keyc]).toLowerCase());
		key.setAttribute(atr, value)
	}
	else{
		if(keyc>=48 && keyc<=58){
			key = document.getElementById(("n"+KEYS[keyc]).toLowerCase());
		}else{
			key = document.getElementById(KEYS[keyc].toLowerCase());
		}
		key.setAttribute(atr, value);
	}
}