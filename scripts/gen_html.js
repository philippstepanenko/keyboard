window.addEventListener("load", init);

const DATA = [{
   "id":"backspace",
   "code":"8",
   "labels":[
      {
         "item":5,
         "label":"Backspace"
      }
   ]
},
{
   "id":"tab",
   "code":"9",
   "labels":[
      {
         "item":5,
         "label":"Tab ↹"
      }
   ]
},
{
   "id":"enter",
   "code":"13",
   "labels":[
      {
         "item":5,
         "label":"↲ Enter" //↵
      }
   ]
},
{
   "id":"lshift",
   "code":"161",
   "labels":[
      {
         "item":5,
         "label":"⇑ Shift"
      }
   ]
},
{
   "id":"lctrl",
   "code":"171",
   "labels":[
      {
         "item":5,
         "label":"Ctrl"
      }
   ]
},
{
   "id":"lalt",
   "code":"181",
   "labels":[
      {
         "item":5,
         "label":"Alt"
      }
   ]
},
{
   "id":"rshift",
   "code":"162",
   "labels":[
      {
         "item":5,
         "label":"⇑ Shift"
      }
   ]
},
{
   "id":"rctrl",
   "code":"172",
   "labels":[
      {
         "item":5,
         "label":"Ctrl"
      }
   ]
},
{
   "id":"ralt",
   "code":"182",
   "labels":[
      {
         "item":5,
         "label":"Alt"
      }
   ]
},
{
   "id":"pause_break",
   "code":"19",
   "labels":[
      {
         "item":5,
         "label":"Pause"
      }
   ]
},
{
   "id":"caps_lock",
   "code":"20",
   "labels":[
      {
         "item":5,
         "label":"Caps Lock"
      }
   ]
},
{
   "id":"escape",
   "code":"27",
   "labels":[
      {
         "item":5,
         "label":"Esc"
      }
   ]
},
{
   "id":"spacebar",
   "code":"32",
   "labels":[
      {
         "item":5,
         "label":" "
      }
   ]
},
{
   "id":"page_up",
   "code":"33",
   "labels":[
      {
         "item":5,
         "label":"PgUp"
      }
   ]
},
{
   "id":"page_down",
   "code":"34",
   "labels":[
      {
         "item":5,
         "label":"PgDn"
      }
   ]
},
{
   "id":"end",
   "code":"35",
   "labels":[
      {
         "item":5,
         "label":"End"
      }
   ]
},
{
   "id":"home",
   "code":"36",
   "labels":[
      {
         "item":5,
         "label":"Home"
      }
   ]
},
{
   "id":"left_arrow",
   "code":"37",
   "labels":[
      {
         "item":5,
         "label":"←" //◄
      }
   ]
},
{
   "id":"up_arrow",
   "code":"38",
   "labels":[
      {
         "item":5,
         "label":"↑" //▲
      }
   ]
},
{
   "id":"right_arrow",
   "code":"39",
   "labels":[
      {
         "item":5,
         "label":"→" //►
      }
   ]
},
{
   "id":"down_arrow",
   "code":"40",
   "labels":[
      {
         "item":5,
         "label":"↓" //▼
      }
   ]
},
{
   "id":"insert",
   "code":"45",
   "labels":[
      {
         "item":5,
         "label":"Ins"
      }
   ]
},
{
   "id":"delete",
   "code":"46",
   "labels":[
      {
         "item":5,
         "label":"Del"
      }
   ]
},
{
   "id":"n0",
   "code":"48",
   "labels":[
      {
         "item":1,
         "label":")"
      },
      {
         "item":2,
         "label":""
      },
      {
         "item":3,
         "label":"0"
      }
   ]
},
{
   "id":"n1",
   "code":"49",
   "labels":[
      {
         "item":1,
         "label":"!"
      },
      {
         "item":3,
         "label":"1"
      }
   ]
},
{
   "id":"n2",
   "code":"50",
   "labels":[
      {
         "item":1,
         "label":"@"
      },
      {
         "item":2,
         "label":"\""
      },
      {
         "item":3,
         "label":"2"
      }
   ]
},
{
   "id":"n3",
   "code":"51",
   "labels":[
      {
         "item":1,
         "label":"#"
      },
      {
         "item":2,
         "label":"№"
      },
      {
         "item":3,
         "label":"3"
      }
   ]
},
{
   "id":"n4",
   "code":"52",
   "labels":[
      {
         "item":1,
         "label":"$"
      },
      {
         "item":2,
         "label":";"
      },
      {
         "item":3,
         "label":"4"
      }
   ]
},
{
   "id":"n5",
   "code":"53",
   "labels":[
      {
         "item":1,
         "label":"%"
      },
      {
         "item":3,
         "label":"5"
      }
   ]
},
{
   "id":"n6",
   "code":"54",
   "labels":[
      {
         "item":1,
         "label":"^"
      },
      {
         "item":2,
         "label":":"
      },
      {
         "item":3,
         "label":"6"
      }
   ]
},
{
   "id":"n7",
   "code":"55",
   "labels":[
      {
         "item":1,
         "label":"&"
      },
      {
         "item":3,
         "label":"7"
      },
      {
         "item":4,
         "label":"?"
      }
   ]
},
{
   "id":"n8",
   "code":"56",
   "labels":[
      {
         "item":1,
         "label":"*"
      },
      {
         "item":3,
         "label":"8"
      }
   ]
},
{
   "id":"n9",
   "code":"57",
   "labels":[
      {
         "item":1,
         "label":"("
      },
      {
         "item":3,
         "label":"9"
      }
   ]
},
{
   "id":"a",
   "code":"65",
   "labels":[
      {
         "item":1,
         "label":"A"
      },
      {
         "item":4,
         "label":"Ф"
      }
   ]
},
{
   "id":"b",
   "code":"66",
   "labels":[
      {
         "item":1,
         "label":"B"
      },
      {
         "item":4,
         "label":"И"
      }
   ]
},
{
   "id":"c",
   "code":"67",
   "labels":[
      {
         "item":1,
         "label":"C"
      },
      {
         "item":4,
         "label":"С"
      }
   ]
},
{
   "id":"d",
   "code":"68",
   "labels":[
      {
         "item":1,
         "label":"D"
      },
      {
         "item":4,
         "label":"В"
      }
   ]
},
{
   "id":"e",
   "code":"69",
   "labels":[
      {
         "item":1,
         "label":"E"
      },
      {
         "item":4,
         "label":"У"
      }
   ]
},
{
   "id":"f",
   "code":"70",
   "labels":[
      {
         "item":1,
         "label":"F"
      },
      {
         "item":4,
         "label":"А"
      }
   ]
},
{
   "id":"g",
   "code":"71",
   "labels":[
      {
         "item":1,
         "label":"G"
      },
      {
         "item":4,
         "label":"П"
      }
   ]
},
{
   "id":"h",
   "code":"72",
   "labels":[
      {
         "item":1,
         "label":"H"
      },
      {
         "item":4,
         "label":"Р"
      }
   ]
},
{
   "id":"i",
   "code":"73",
   "labels":[
      {
         "item":1,
         "label":"I"
      },
      {
         "item":4,
         "label":"Ш"
      }
   ]
},
{
   "id":"j",
   "code":"74",
   "labels":[
      {
         "item":1,
         "label":"J"
      },
      {
         "item":4,
         "label":"О"
      }
   ]
},
{
   "id":"k",
   "code":"75",
   "labels":[
      {
         "item":1,
         "label":"K"
      },
      {
         "item":4,
         "label":"Л"
      }
   ]
},
{
   "id":"l",
   "code":"76",
   "labels":[
      {
         "item":1,
         "label":"L"
      },
      {
         "item":4,
         "label":"Д"
      }
   ]
},
{
   "id":"m",
   "code":"77",
   "labels":[
      {
         "item":1,
         "label":"M"
      },
      {
         "item":4,
         "label":"Ь"
      }
   ]
},
{
   "id":"n",
   "code":"78",
   "labels":[
      {
         "item":1,
         "label":"N"
      },
      {
         "item":4,
         "label":"Т"
      }
   ]
},
{
   "id":"o",
   "code":"79",
   "labels":[
      {
         "item":1,
         "label":"O"
      },
      {
         "item":4,
         "label":"Щ"
      }
   ]
},
{
   "id":"p",
   "code":"80",
   "labels":[
      {
         "item":1,
         "label":"P"
      },
      {
         "item":4,
         "label":"З"
      }
   ]
},
{
   "id":"q",
   "code":"81",
   "labels":[
      {
         "item":1,
         "label":"Q"
      },
      {
         "item":4,
         "label":"Й"
      }
   ]
},
{
   "id":"r",
   "code":"82",
   "labels":[
      {
         "item":1,
         "label":"R"
      },
      {
         "item":4,
         "label":"К"
      }
   ]
},
{
   "id":"s",
   "code":"83",
   "labels":[
      {
         "item":1,
         "label":"S"
      },
      {
         "item":4,
         "label":"Ы"
      }
   ]
},
{
   "id":"t",
   "code":"84",
   "labels":[
      {
         "item":1,
         "label":"T"
      },
      {
         "item":4,
         "label":"Е"
      }
   ]
},
{
   "id":"u",
   "code":"85",
   "labels":[
      {
         "item":1,
         "label":"U"
      },
      {
         "item":4,
         "label":"Г"
      }
   ]
},
{
   "id":"v",
   "code":"86",
   "labels":[
      {
         "item":1,
         "label":"V"
      },
      {
         "item":4,
         "label":"М"
      }
   ]
},
{
   "id":"w",
   "code":"87",
   "labels":[
      {
         "item":1,
         "label":"W"
      },
      {
         "item":4,
         "label":"Ц"
      }
   ]
},
{
   "id":"x",
   "code":"88",
   "labels":[
      {
         "item":1,
         "label":"X"
      },
      {
         "item":4,
         "label":"Ч"
      }
   ]
},
{
   "id":"y",
   "code":"89",
   "labels":[
      {
         "item":1,
         "label":"Y"
      },
      {
         "item":4,
         "label":"Н"
      }
   ]
},
{
   "id":"z",
   "code":"90",
   "labels":[
      {
         "item":1,
         "label":"Z"
      },
      {
         "item":4,
         "label":"Я"
      }
   ]
},
{
   "id":"left_window",
   "code":"91",
   "labels":[
      {
         "item":5,
         "label":"❖" //⊞
      }
   ]
},
{
   "id":"right_window",
   "code":"92",
   "labels":[
      {
         "item":5,
         "label":"❖" //⊞
      }
   ]
},
{
   "id":"select",
   "code":"93",
   "labels":[
      {
         "item":5,
         "label":"Select"
      }
   ]
},
{
   "id":"f1",
   "code":"112",
   "labels":[
      {
         "item":5,
         "label":"F1"
      }
   ]
},
{
   "id":"f2",
   "code":"113",
   "labels":[
      {
         "item":5,
         "label":"F2"
      }
   ]
},
{
   "id":"f3",
   "code":"114",
   "labels":[
      {
         "item":5,
         "label":"F3"
      }
   ]
},
{
   "id":"f4",
   "code":"115",
   "labels":[
      {
         "item":5,
         "label":"F4"
      }
   ]
},
{
   "id":"f5",
   "code":"116",
   "labels":[
      {
         "item":5,
         "label":"F5"
      }
   ]
},
{
   "id":"f6",
   "code":"117",
   "labels":[
      {
         "item":5,
         "label":"F6"
      }
   ]
},
{
   "id":"f7",
   "code":"118",
   "labels":[
      {
         "item":5,
         "label":"F7"
      }
   ]
},
{
   "id":"f8",
   "code":"119",
   "labels":[
      {
         "item":5,
         "label":"F8"
      }
   ]
},
{
   "id":"f9",
   "code":"120",
   "labels":[
      {
         "item":5,
         "label":"F9"
      }
   ]
},
{
   "id":"f10",
   "code":"121",
   "labels":[
      {
         "item":5,
         "label":"F10"
      }
   ]
},
{
   "id":"f11",
   "code":"122",
   "labels":[
      {
         "item":5,
         "label":"F11"
      }
   ]
},
{
   "id":"f12",
   "code":"123",
   "labels":[
      {
         "item":5,
         "label":"F12"
      }
   ]
},
{
   "id":"semi-colon",
   "code":"186",
   "labels":[
      {
         "item":1,
         "label":":"
      },
      {
         "item":3,
         "label":";"
      },
      {
         "item":4,
         "label":"Ж"
      }
   ]
},
{
   "id":"equal_sign",
   "code":"187",
   "labels":[
      {
         "item":1,
         "label":"+"
      },
      {
         "item":3,
         "label":"="
      }
   ]
},
{
   "id":"comma",
   "code":"188",
   "labels":[
      {
         "item":1,
         "label":"<"
      },
      {
         "item":3,
         "label":","
      },
      {
         "item":4,
         "label":"<"
      }
   ]
},
{
   "id":"dash",
   "code":"189",
   "labels":[
      {
         "item":1,
         "label":"_"
      },
      {
         "item":3,
         "label":"-"
      }
   ]
},
{
   "id":"period",
   "code":"190",
   "labels":[
      {
         "item":1,
         "label":">"
      },
      {
         "item":3,
         "label":"."
      },
      {
         "item":4,
         "label":"Ю"
      }
   ]
},
{
   "id":"forward_slash",
   "code":"191",
   "labels":[
      {
         "item":1,
         "label":"?"
      },
      {
         "item":2,
         "label":","
      },
      {
         "item":3,
         "label":"/"
      },
      {
         "item":4,
         "label":"."
      }
   ]
},
{
   "id":"grave_accent",
   "code":"192",
   "labels":[
      {
         "item":1,
         "label":"~"
      },
      {
         "item":3,
         "label":"`"
      },
      {
         "item":4,
         "label":"Ё"
      }
   ]
},
{
   "id":"open_bracket",
   "code":"219",
   "labels":[
      {
         "item":1,
         "label":"{"
      },
      {
         "item":3,
         "label":"["
      },
      {
         "item":4,
         "label":"Х"
      }
   ]
},
{
   "id":"back_slash",
   "code":"220",
   "labels":[
      {
         "item":1,
         "label":"|"
      },
      {
         "item":2,
         "label":"/"
      },
      {
         "item":3,
         "label":"\\"
      },
      {
         "item":4,
         "label":"\\"
      }
   ]
},
{
   "id":"close_bracket",
   "code":"221",
   "labels":[
      {
         "item":1,
         "label":"}"
      },
      {
         "item":3,
         "label":"]"
      },
      {
         "item":4,
         "label":"Ъ"
      }
   ]
},
{
   "id":"single_quote",
   "code":"222",
   "labels":[
      {
         "item":1,
         "label":"\""
      },
      {
         "item":3,
         "label":"'"
      },
      {
         "item":4,
         "label":"Э"
      }
   ]
}
];

let test = `backspace	8	5backspace
tab	9	5Tab
enter	13	5Enter
lshift	161	4Shift
lctrl	171	5Ctrl
lalt	181	5Alt
rshift	162	5Shift
rctrl	172	5Ctrl
ralt	182	5Alt
pause_break	19	5Pause
caps_lock	20	5Caps Lock
escape	27	5Esc
spacebar	32	5 
page_up	33	1PgUp
page_down	34	1PgDn
end	35	1End
home	36	1Home
left_arrow	37	5Left
up_arrow	38	5Up
right_arrow	39	5Right
down_arrow	40	5Down
insert	45	5Ins
delete	46	5Del
n0	48	1)	2	30
n1	49	1!	31
n2	50	1@	2\\"	32
n3	51	1#	2№	33
n4	52	1$	2;	34
n5	53	1%	35
n6	54	1^	2:	36
n7	55	1&	37	4?
n8	56	1*	38
n9	57	1(	39
A	65	1A	4Ф
B	66	1B	4И
C	67	1C	4С
D	68	1D	4В
E	69	1E	4У
F	70	1F	4А
G	71	1G	4П
H	72	1H	4Р
I	73	1I	4Ш
J	74	1J	4О
K	75	1K	4Л
L	76	1L	4Д
M	77	1M	4Ь
N	78	1N	4Т
O	79	1O	4Щ
P	80	1P	4З
Q	81	1Q	4Й
R	82	1R	4К
S	83	1S	4Ы
T	84	1T	4Е
U	85	1U	4Г
V	86	1V	4М
W	87	1W	4Ц
X	88	1X	4Ч
Y	89	1Y	4Н
Z	90	1Z	4Я
left_window	91	5LWin
right_window	92	5RWin
select	93	5Select
numpad_0	96	50
numpad_1	97	51
numpad_2	98	52
numpad_3	99	53
numpad_4	100	54
numpad_5	101	55
numpad_6	102	56
numpad_7	103	57
numpad_8	104	58
numpad_9	105	59
multiply	106	5*
add	107	5+
subtrack	109	5-
decimal_point	110	5.
divide	111	5/
f1	112	5F1
f2	113	5F2
f3	114	5F3
f4	115	5F4
f5	116	5F5
f6	117	5F6
f7	118	5F7
f8	119	5F8
f9	120	5F9
f10	121	5F10
f11	122	5F11
f12	123	5F12
num_lock	144	5NumLk
scroll_lock	145	5ScrLk
semi-colon	186	1:	3;	4Ж
equal_sign	187	1+	3=
comma	188	1<	3,	4<
dash	189	1_	3-
period	190	1>	3.	4Ю
forward_slash	191	1?	2,	3/	4.
grave_accent	192	1~	3\`	4Ё
open_bracket	219	1{	3[	4Х
back_slash	220	1|	2/	3\\\\	4\\\\
close_bracket	221	1}	3]	4Ъ
single_quote	222	1\\"	3'	4Э`;

function parse_t(data){
	//let a = Array.prototype.concat.apply([], data.split("\n").map((i) => i.split("	")) );
	let a = Array.prototype.concat.apply([], data.split("\n") );
	let str = "";
	a.forEach((item, i, a) => {
		let b  = item.split("	");
		// если не первая строка
		str += i > 0 && i < a.length ? `,{` : `{`;
      //str="";
      //str+=`{`;

		b.forEach((jtem, j, b) =>{
			const K = ["id", "code"]; // все обязательные свойства до labels
			if (j < K.length) str += `"${K[j]}":"${b[j].toLowerCase()}",`;
			if (j >= K.length){
				// добавления ключа labels для массива обозначений клавиш
				if (j == K.length) str +=`"labels":[`;
				// добавление обозначения клавиши в массив по ключу labels
				str +=`{"item":${b[j].substr(0,1)},"label":"${b[j].substr(1)}"}`;
				// текущий элемент является последним в массиве?
				str += (j + 1 == b.length) ? `]}` : `,`;
			}
		});
		//DATA.push(JSON.parse(str));
	});
	return str;
}

function init(){
	const ttt = parse_t(test);
	//let r = document.getElementById('res');
	//r.innerHTML = ttt;
	console.log(ttt);
	//console.log(DATA);
}